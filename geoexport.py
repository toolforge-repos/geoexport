#!/usr/bin/python3

import cgi
import urllib
import json
import sys
import toolforge
import pymysql
import requests
import urllib.request
import os

class GeoExport:
#	_project = ""  # "en.wikipedia.org"
	_cpstr = "coprimary=primary"
	_gpxheader ='<?xml version="1.0" encoding="UTF-8"?>\n' + '<gpx xmlns="http://www.topografix.com/GPX/1/1" >'
	def __init__(self, args, debug=False):
		self._args = args
		self.data = []
		self._project = project = args.get("project")
		titles = args.get("titles")
		coprimarg = args.get("coprimary")
		if (self._project is None):
			self._project = "en.wikipedia.org"
		print(self._project, project)
		self._debug = debug
		self._apiurl = "https://" + self._project + "/w/api.php?format=json"
		
	def run(self):
		args = self._args
		titles = args.get("titles")
		titles = titles.replace(" ", "_")
		coprimarg = args.get("coprimary")
	
		pageids = []
		url = self._apiurl + "&action=query&prop=info&inprop=url|talkid|subjectid&titles=" + titles
		print(url)
		catids = []
		cl_tos = []  # category titles
		pageids = []
		data = urllib.request.urlopen(url)
		dataStr = (data.read()).decode(data.headers.get_content_charset('utf-8'))
		dataDict = (json.loads(dataStr))
		if (coprimarg is not None):
			self._cpstr = "coprimary=" + coprimarg
		if self._debug:
			print(args)
			print(coprimarg, self._cpstr)
			print(dataStr)
			print("pageinfo", dataDict)
		# https://www.mediawiki.org/wiki/Help:Namespaces#Localisation
		for page in dataDict['query']['pages']:
			pageDict = dataDict['query']['pages'][str(page)]

			ns = pageDict['ns']
			# 14	Category
			if ns == 14:
				catids.append(pageDict['pageid'])
				cl_tos.append(pageDict['title'].replace('Category:', "", 1).replace(' ', '_'))
			#.removeprefix('Category:'))
			# 1	Talk
			elif ns == 1:
				pageids.append(pageDict['subjectid'])
			# 0	(Main)
			else:
				pageids.append(pageDict['pageid'])				
		print("ids:", "catids:", catids, "pageids:", pageids, cl_tos)

		output = ""
		print(len(pageids),len(catids) , output)
	# 	if (len(pageids) == 1 and len(catids) == 0):
	# 		print("len(pageids) == 1")
	# #			print(type(x))
		output = self.processPages(dataDict, output, 0)
			
		cl_to_str = '("' + str.join('","', cl_tos) + '")'
		print("cl_to_str", cl_to_str)

		project_db = self.get_project_db(self._project)
		innerquery = '(Select cl_from from categorylinks  WHERE cl_to IN ' + cl_to_str + ')'
		conn = toolforge.connect(project_db) # You can also use "enwiki_p"
		# conn is a pymysql.connection object.
		with conn.cursor(pymysql.cursors.DictCursor) as cur:
			x = cur.execute('SELECT COUNT(*) FROM page Where page_id in ' + innerquery + ' and page_namespace=0;')  # Or something.
			#print(cur.fetchone())
			#print(cur.fetchall()[0][0])
			mainlen = cur.fetchall()[0]['COUNT(*)']
			if self._debug:
				print("mainlen", mainlen)
				print(type(mainlen))

		if self._debug:
			print("mainlen", mainlen)
			
		titlestrarr = []

		# conn = toolforge.connect('enwiki') # You can also use "enwiki_p"
		# conn is a pymysql.connection object.

		resultslist = self.get_cat_non_main(conn, innerquery)
		if resultslist is not None:
			pageids.extend(resultslist)

		# print('<?xml version="1.0" encoding="UTF-8"?>')
		# print('<gpx xmlns="http://www.topografix.com/GPX/1/1" >')

		# functionise this
		
		print(titlestrarr)
		output = self.run_pageids(output, pageids) + "\n</gpx>"
#		return (self.run_pageids(output, pageids))
		return self._gpxheader + output

	def get_project_db(self, project):
		conn = toolforge.connect('meta')
		with conn.cursor(pymysql.cursors.DictCursor) as cur:
			query = 'SELECT dbname  from wiki where url like "%/' + project + '%"'
			cur.execute(query)
			result = cur.fetchone()
			print(result)
			conn.close()
			return result['dbname']

	def get_cat_non_main(self, conn, innerquery):
		with conn.cursor(pymysql.cursors.DictCursor) as cur:
			query = 'SELECT page_id FROM page WHERE page_title in (SELECT page_title FROM  page Where page_id in ' + innerquery + ' and page_namespace!=0) and page_namespace=0'
			if self._debug:
				print(query)
			cur.execute(query)
			results = (cur.fetchall())
			if (len(results) > 0):
				resultslist = [item['page_id'] for item in results]
				# pageids.extend(resultslist)
				if self._debug:
					print("results", results)
					print("resultslist",  resultslist)
				return resultslist
				
	def writePoint(self, coordDict, title):
		gpxstr = '<wpt lat="' + str(coordDict['lat']) + '" lon="' + str(coordDict['lon']) + '">'
		name = title
		if 'name' in coordDict:
			name = coordDict['name']
		gpxstr = gpxstr + "\n\t\t<name>" + name + "</name>"
		gpxstr = gpxstr + "\n\t\t<desc>" + title + "</desc>"
		linkhref = "https://" + self._project + "/wiki/" + title
		gpxstr = gpxstr + '\n\t\t<link href="' + linkhref + '" >' + linkhref + "</link>"
		# 		print(coordDict)
		gpxstr = gpxstr + "\n\t</wpt>"
		return (gpxstr)

	def parsePage(self, pageDict):
		gpxstr = ""
		title = pageDict['title']
		if 'coordinates' in pageDict.keys():
			for coordDict in pageDict['coordinates']:
				gpxstr = gpxstr + '\n\t' + self.writePoint(coordDict, title)
		return gpxstr

	def get_pages(self, pageid_chunk, output, current_level):
		format = "&format=json"
		print("get_pages", pageid_chunk)
		pageidstr = ('|'.join(map(str, pageid_chunk)))
		print("get_pages", pageidstr)
		#caturl="https://" + self._project + "/w/api.php?" + "action=query&prop=coordinates&"+ cpstr +"&coprop=name|type&colimit=500&pageids=" + pageidstr
		caturl = self._apiurl +"&action=query&prop=coordinates&"+ self._cpstr +"&coprop=name|type&colimit=500&pageids=" + pageidstr
		print("'"+caturl+"'")
		dataDict = json.load(urllib.request.urlopen(caturl))
		continueid = ""
		if ("continue" in dataDict.keys()):
			print("continue", dataDict.keys())
		output = self.processPages(dataDict, output, current_level)
		return output

	def run_pageids(self, output, pageids):
		n = 50
		# print("pageids", pageids)

		pageids50 = [pageids[i:i + n] for i in range(0, len(pageids), n)]
		print("pageids50", pageids50)
		for pageid_chunk in pageids50:
			pageidstr = "|".join(map(str, pageid_chunk))
			output= self.get_pages(pageid_chunk, output, 0)
		return output

	def processPages(self, dataDict, output, current_level):
		if self._debug:
			print("processPages")
			
			


		for page in dataDict['query']['pages']:
			pageID = str(page)
			pageDict = dataDict['query']['pages'][pageID]
			if pageDict['ns'] == 0:  # 0 (Main)
				output = output + self.parsePage(pageDict)
			elif pageDict['ns'] == 14:  # 14 Category
	#			url = "&action=query&list=categorymembers&cmlimit=500&&cmpageid="+pageID
				print(pageID)
				output = output + self.get_cat(pageID, output, current_level + 1)
				# catData = urllib.request.urlopen(url)
				# catDict = (json.load(catData))
				# print(catDict)
			else:
				print("processPages pageDict", current_level, pageDict)
				output = output + self.parsePage(pageDict)
			
		return output

	def get_cat(self, gcmpageid, output, current_level):
		caturl = self._apiurl + "&action=query&generator=categorymembers&prop=coordinates&"+self._cpstr + "&coprop=name|type&colimit=500&gcmlimit=500&format=json&gcmpageid=" + gcmpageid
		if self._debug:
			print(caturl)
		dataDict = json.load(urllib.request.urlopen(caturl))
		continueid = ""
		if ("continue" in dataDict.keys()):
			print("continue", dataDict.keys())
		output = self.processPages(dataDict, output, current_level)
		return output


def run(args, debug=False):
	ge = GeoExport(args, True)
	return (ge.run())


	# https://en.wikipedia.org/w/api.php?action=query&prop=coordinates&titles=Lota%2C+Queensland%7CBrisbane&format=json&prop=coordinates&coprimary=all&coprop=name|type&colimit=500

	# for article in articlelist:
	# https://en.wikipedia.org/w/api.php?action=query&generator=categorymembers&gcmlimit=500&gcmtitle=Category:Suburbs_of_the_City_of_Brisbane&prop=coordinates&coprimary=all&coprop=name|type&colimit=500&format=json&coprimary=all&coprop=name|type


	# https://www.mediawiki.org/wiki/API:Categorymembers
	# https://en.wikipedia.org/wiki/Special:ApiSandbox#action=query&format=json&prop=coordinates&list=&generator=categorymembers&colimit=500&cocontinue=4643895%7C789151824&coprimary=all&gcmtitle=Category%3ASuburbs_of_the_City_of_Brisbane&gcmlimit=500
	# https://www.mediawiki.org/wiki/API:Info
	# https://www.mediawiki.org/wiki/Extension:GeoData#prop=coordinates
	# https://petscan.wmflabs.org/?categories=Railway+stations+in+Queensland&depth=9999&doit=&format=json
	# https://petscan.wmflabs.org/?categories=Railway_stations_in_Queensland&depth=9999&doit=&format=json&ns[14]=1
	
def main():
	debug = False
	environ = os.environ
	qs = ""
	if 'QUERY_STRING' in environ:
		qs = environ['QUERY_STRING']
	else:
		if sys.argv[1:]:
			qs = sys.argv[1]
		else:
			qs = ""
			environ['QUERY_STRING'] = qs    # XXX Shouldn't, really	
	params = dict(urllib.parse.parse_qsl(qs))
	# cgi.parse()
	print(params)
	format = ""
	cpstr = ""
	# print("content-type: application/gpx+xml\n\n")
	try:
		if 'debug' in params:
			debug = True
			print("debug")

	except KeyError:
		print("KeyError")
	try:
		formatp = params['format']
		if formatp == "gpx":
			format = "application/gpx+xml"
		if 'coprimary' in params:
			cpstr = "coprimary=" + params['coprimary']
			print(cpstr)


	# 	print(params)

	# if 'format' in params:
	# 	if params['format'] == 'gpx':
	# 		print("content-type: application/gpx+xml\n\n")
	# else:
	# 	print("xxx")

	# try:
	# 	match params['format'][0]:
	# 		case 'gpx':
	# 			format = "application/gpx+xml"

	except KeyError:
		print("content-type: text/html\n\n")
		print("KeyError")
	# 	print("content-type: text/html\n\n")
	# 	print("Content-type: application/gpx+xml\n\n")
	print("content-type: " + format + "\n\n")
	# print("content-type: text/html\n\n")

	articlelist = params['titles']
	if debug:
		print(params)
		print("test")
		print(articlelist)  # , file = sys.stderr)
		print("<br />")

	titlelist = "|".join(articlelist)
	# print(titles)
	''' 
	https://www.mediawiki.org/wiki/Extension:GeoData#prop=coordinates
	https://en.wikipedia.org/w/api.php?action=query&prop=coordinates&titles=Cannon_Hill%2C_Queensland&coprimary=all&coprop=name

	'''
	
	output = run(params,True)
	print (output)
	
	# run("en.wikipedia.org", titlelist)
	

if __name__ == "__main__":
    main()
