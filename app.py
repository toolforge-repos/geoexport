#ecoding: utf-8 -*-
#
# This file is part of the Toolforge flask WSGI tutorial
#
# Copyright (C) 2017 Bryan Davis and contributors
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
# more details.
#
# You should have received a copy of the GNU General Public License along
# with this program.  If not, see <http://www.gnu.org/licenses/>.

import flask
import geoexport

app = flask.Flask(__name__)




@app.route('/')
def index():
    return 'Hello World!'


@app.route('/test')
def test():
    return flask.request.args


@app.route('/gpx')
def gpx():
    debug = False
    args = flask.request.args
    project = args.get("project")
    titles = args.get("titles")

    if project == None:
        project = "en.wikipedia.org"
    article = args.get("article")
    if article == None:
        article = "Category:Wikipedia_requested_photographs_in_Brisbane"
    if "debug" in args:
        debug = True
#    output = geoexport.run(project, article)
    output = geoexport.run(args, debug)
    if "debug" in args:
        return output
    return flask.Response(output, mimetype='application/gpx+xml', headers={"Content-disposition":"attachment; filename="+titles+".gpx"})


#  argu = flask.request.args.get("article")
#  return argu + 'gpx'


